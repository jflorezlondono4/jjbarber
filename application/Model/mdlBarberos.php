<?php

namespace Mini\Model;

use Mini\Core\Model;

class mdlBarberos extends Model
{

    private $IdBarbero;
    private $Nombre;
    private $Cedula;
    private $Edad;
    private $Genero;
    private $Foto;
    private $HoraInicio;
    private $HoraFin;

    public function __SET($attr, $value){
        $this->$attr=$value;
    }
    public function __GET($attr){
    return  $this->$attr;
    }

        function __construct()
    {
        try {
            parent::__construct();
        } catch (PDOException $e) {
            exit("error en la conexion.");
        }
        
    }

    public function RegistrarBarbero()
    {

        $sql ="CALL JJ_RegistrarBarbero(?,?,?,?,?,?,?)";
        $stm = $this->db->prepare($sql);
        $stm->bindparam(1, $this->Nombre);
        $stm->bindparam(2, $this->Cedula);
        $stm->bindparam(3, $this->Edad);
        $stm->bindparam(4, $this->Genero);
        $stm->bindparam(5, $this->Foto);
        $stm->bindparam(6, $this->HoraInicio);
        $stm->bindparam(7, $this->HoraFin);
        $stm->execute();

    }

        public function EditarBarbero()
    {

        $sql = "CALL JJ_EditarBarbero(?,?,?,?,?,?,?,?)";
        $stm = $this->db->prepare($sql);
        $stm->bindparam(1, $this->IdBarbero);
        $stm->bindparam(2, $this->Nombre);
        $stm->bindparam(3, $this->Cedula);
        $stm->bindparam(4, $this->Edad);
        $stm->bindparam(5, $this->Genero);
        $stm->bindparam(6, $this->Foto);
        $stm->bindparam(7, $this->HoraInicio);
        $stm->bindparam(8, $this->HoraFin);
        $stm->execute();

    }

        public function ListarBarbero()
    {
        $sql = "CALL JJ_ListarBarbero()";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchall();
    }

    public function ConsultarBarbero()
    {
        $sql = "CALL JJ_ConsultarBarberoId(?)";
        $stm = $this->db->prepare($sql);
        $stm->bindparam(1, $this->IdBarbero);
        $stm->execute();
        return $stm->fetch();
    }

    public function ConsultarBarberoNombre()
    {
        $sql = "CALL JJ_ConsultarBarbero(?)";
        $stm = $this->db->prepare($sql);
        $stm->bindparam(1, $this->Nombre);
        $stm->execute();
        return $stm->fetchall();
    }

    
}
