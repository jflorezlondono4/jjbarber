<div class="row">
  <div class="col-md-1"></div>
  <div class="col-lg-10">
   <div class="widget-header"> <i class="icon-hand-up"></i>
              <h3>Barberos</h3>
            </div>
  <div class="widget-content">
              <table class="table">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Foto</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($datos as $value): ?>
                  <tr>
                    <td><?=$value["Nombre_Barbero"]?></td>
                    <td><?=$value["Foto_Barbero"]?></td>
                    <td><a href="<?php echo URL; ?>Barberos/edit/<?=$value["IdBarbero"] ?>">Editar</a></td>
                  </tr>

                <?php endforeach; ?>
  
                </tbody>
              </table>
            </div>
     </div>
  </div>