<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <meta name="description" content="">
	  <meta name="author" content="">

	  <title>Business Frontpage - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo URL; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo URL; ?>css/business-frontpage.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>bootstrap/css/jquery.datetimepicker.css"/>

  <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>bootstrap/css/barra-busqueda.css"/>

</head>
<body style="background-image: url(<?php echo URL; ?>img/fondo8.jpg)">
    <!-- logo, check the CSS file for more info how the logo "image" is shown -->

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="<?php echo URL; ?>Home/index">JJBarber</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="#">Cerrar sesión</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
