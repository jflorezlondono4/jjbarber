<?php  

namespace Mini\Controller;
use Mini\Model\mdlBarberos;
/**
* 
*/

class BarberosController 
{
	function __construct(){
        $this ->mdlBarberos =  new mdlBarberos(); 

	}
	

	public function index()
    {

        require APP . 'view/_templates/header.php';
        require APP . 'view/Barbero/RegistrarBarbero.php';
        require APP . 'view/_templates/footer.php';
    }

    public function registrarBarbero()
    {

        $fechainicio = $_POST['horaini'];
        $fechainicio_conv = strftime('%Y-%m-%dT%H:%M:%S', strtotime($fechainicio));

        $fechafin = $_POST['horafin'];
        $fechafin_conv = strftime('%Y-%m-%dT%H:%M:%S', strtotime($fechafin));

//--------------
        $this ->mdlBarberos->__SET("Nombre",$_POST['nombre']);
        $this ->mdlBarberos->__SET("Cedula",$_POST['cedula']);
        $this ->mdlBarberos->__SET("Edad",$_POST['edad']);
        $this ->mdlBarberos->__SET("Genero",$_POST['genero']);
        $this ->mdlBarberos->__SET("Foto",$_POST['foto']);
        $this ->mdlBarberos->__SET("HoraInicio",$fechainicio_conv);
        $this ->mdlBarberos->__SET("HoraFin",$fechafin_conv);
        $this ->mdlBarberos->RegistrarBarbero();
        header("location:".URL.'Barberos/index');
    }

    public function listarBarberos()
    {
        $datos = $this->mdlBarberos->ListarBarbero();
        require APP . 'view/_templates/header.php';
        require APP . 'view/Barbero/ListarBarberos.php';
        require APP . 'view/_templates/footer.php';
    }

        public function edit($codi){

         $this ->mdlBarberos->__SET("IdBarbero",$codi);
         $datos= $this ->mdlBarberos->ConsultarBarbero();

        require APP . 'view/_templates/header.php';
        require APP . 'view/Barbero/EditarBarbero.php';
        require APP . 'view/_templates/footer.php';

        }

        public function consultarBarberoNombre(){

         $validar = $_POST['nombre'];
         if (!empty($validar)) {

             $this ->mdlBarberos->__SET("Nombre",$_POST['nombre']);
             $datos= $this ->mdlBarberos->ConsultarBarberoNombre();
         
             if ($datos == null) {
                 // var_dump($datos);
                //ALERTA INDICANDO QUE EL BARBERO NO EXISTE
        require APP . 'view/_templates/header.php';
        require APP . 'view/Home/index.php';
        require APP . 'view/_templates/footer.php';
             }

        require APP . 'view/_templates/header.php';
        require APP . 'view/Barbero/ConsultarBarbero.php';
        require APP . 'view/_templates/footer.php';

         }else{

        require APP . 'view/_templates/header.php';
        require APP . 'view/Home/index.php';
        require APP . 'view/_templates/footer.php';
         }
         

        }

        public function editarBarbero()
        {
        $fechainicio = $_POST['horaini'];
        $fechainicio_conv = strftime('%Y-%m-%dT%H:%M:%S', strtotime($fechainicio));

        $fechafin = $_POST['horafin'];
        $fechafin_conv = strftime('%Y-%m-%dT%H:%M:%S', strtotime($fechafin));

//--------------
        $this ->mdlBarberos->__SET("IdBarbero",$_POST['codigo']);
        $this ->mdlBarberos->__SET("Nombre",$_POST['nombre']);
        $this ->mdlBarberos->__SET("Cedula",$_POST['cedula']);
        $this ->mdlBarberos->__SET("Edad",$_POST['edad']);
        $this ->mdlBarberos->__SET("Genero",$_POST['genero']);
        $this ->mdlBarberos->__SET("Foto",$_POST['foto']);
        $this ->mdlBarberos->__SET("HoraInicio",$fechainicio_conv);
        $this ->mdlBarberos->__SET("HoraFin",$fechafin_conv);
        $e = $this ->mdlBarberos->EditarBarbero();
        header("location:".URL.'Barberos/index');

        }

    
    //     public function registrar(){
        
    //     $this ->mdlTipoVehiculo->__SET("IdTipoVehiculo",$_POST['codigo']);
    //     $this ->mdlTipoVehiculo->__SET("Nombre",$_POST['nombre']);
    //     $this ->mdlTipoVehiculo->__SET("Estado",$_POST['estado']);
    //     $this ->mdlTipoVehiculo->__SET("Descripcion",$_POST['description']);
    //     $e = $this ->mdlTipoVehiculo->registrarTVehiculo();
    //     header("location:".URL.'tipoVehiculo/tipoVehiculo');
    //     }



    

    //     public function cambiarEstado(){
    //     $this ->mdlTipoVehiculo->__SET("IdTipoVehiculo",$_POST["codigo"]);
    //     $this ->mdlTipoVehiculo->__SET("Estado",$_POST["estado"]);
    //     $datos= $this ->mdlTipoVehiculo->cambiarEstado();
    //     if ($datos) {
    //        echo json_encode(["b"=>1]);
    //     }else{
    //         echo json_encode(["b"=>0]);
    //     }
    //     // header("location:".URL."UnidadMedida/unidadMedida");
    // }

    
}